package ca.qc.cvm.dba.persinteret.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.Binary;
import org.neo4j.driver.Record;
import org.neo4j.driver.Session;
import org.neo4j.driver.StatementResult;
import org.neo4j.driver.types.Node;
import org.neo4j.driver.types.Relationship;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import ca.qc.cvm.dba.persinteret.entity.Person;


import java.util.Date;



public class PersonDAO {

	final static String DATE_FORMAT = "yyyy-mm-dd";
	/**
	 * M�thode permettant de retourner la liste des personnes de la base de donn�es.
	 * 
	 * Notes importantes:
	 * - N'oubliez pas de limiter les r�sultats en fonction du param�tre limit
	 * - La liste doit �tre tri�es en ordre croissant, selon les noms des personnes
	 * - Le champ de filtre doit permettre de filtrer selon le pr�fixe du nom (insensible � la casse)
	 * - Si le champ withImage est � false, alors l'image (byte[]) n'a pas � faire partie des r�sultats
	 * - N'oubliez pas de mettre l'ID dans la personne, car c'est utile pour savePerson()
	 * - Il pourrait ne pas y avoir de filtre (champ filtre vide)
	 * 
	 * @param filterText champ filtre, peut �tre vide ou null
	 * @param withImage true si l'image est n�cessaire, false sinon.
	 * @param limit permet de restreindre les r�sultats
	 * @return la liste des personnes, selon le filtre si n�cessaire et filtre
	 */
	
	
	public static List<Person> getPeopleList(String filterText, final boolean withImage, int limit) {
		final List<Person> peopleList = new ArrayList<Person>();
		Session session = Neo4jConnection.getConnection();
		Map<String, Object> params = new HashMap<String, Object>();
		MongoDatabase connection = MongoConnection.getConnection();
		MongoCollection<Document> listePersonneCollection = connection.getCollection("personnes");
		
		
		/** Verification de si le filterText est contenu dans un nom**/
		Pattern regex = Pattern.compile(filterText, Pattern.CASE_INSENSITIVE);
		Bson filter = Filters.eq("name", regex);
		FindIterable<Document> itr = listePersonneCollection.find(filter).sort(new Document("name",1)).limit(limit);
		
		try {
			itr.forEach(new Block<Document>() {
				@Override
				public void apply(final Document document) {
					Node P1 = null;
					Person person = new Person();
					person.setId(document.getLong("id"));
					person.setName(document.getString("name"));
					person.setCodeName(document.getString("codeName"));
					person.setDateOfBirth(document.getString("dateOfBirth"));
					person.setStatus(document.getString("status"));
					if (withImage == true) {	
						byte[] imageData = ((Binary)document.get("imageData")).getData();
						person.setImageData(imageData);
					}
					
					if (filterText == "" || filterText == null) {
						
					}
					params.put("p1",person.getName());
					String query = " MATCH (a:person)-[r]->(m:person) WHERE a.name = {p1} RETURN m ";
					ArrayList<String> newConn = new ArrayList<String>();
					StatementResult result = session.run(query,params);
					
					while(result.hasNext()) {
			                Record record = result.next();
			                P1 = record.get("m").asNode();
			                
			                newConn.add(P1.get("name").asString());
			        }
					
					person.setConnexions(newConn);
				    peopleList.add(person);
				}
				
			});	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
				
		
		return peopleList ;
	
	}

	/**
	 * M�thode permettant de sauvegarder une personne
	 * 
	 * Notes importantes:
	 * - Si le champ "id" n'est pas null, alors c'est une mise � jour, pas une insertion
	 * - Le nom de code est optionnel, le reste est obligatoire
	 * - Le nom de la personne doit �tre unique
	 * - Regarder comment est fait la classe Personne pour avoir une id�e des donn�es � sauvegarder
	 * - Pour cette m�thode, d�normaliser pourrait vous �tre utile. La performance des lectures est vitale.
	 * - Je vous conseille de sauvegarder la date de naissance en format long (en millisecondes)
	 * - Une connexion va dans les deux sens. Plus pr��is�ment, une personne X qui connait une autre personne Y
	 *   signifie que cette derni�re connait la personne X
	 * - Attention, les connexions d'une personne pourraient changer avec le temps (ajout/suppression)
	 * - � l'insertion, il doit y avoir un id num�rique unique associ� � la personne. 
	 *   D�pendemment de la base de donn�es, vous devrez trouver une strat�gie pour faire un id num�rique.
	 * 
	 * @param person
	 * @return true si succ�s, false sinon
	 */
	public static boolean save(Person person) {
		boolean success = false;
		
		
		try {
			
			/**Cr�ation des connection et des objets n�cessaires � l'obention d'info sur les DB**/
			Session session = Neo4jConnection.getConnection();
			Map<String, Object> params = new HashMap<String, Object>();
			MongoDatabase connection = MongoConnection.getConnection();
			MongoCollection<Document> collection = connection.getCollection("personnes");
			Node P1 = getPersonNode(person.getName());
			Node P2 = null;
			
			/**Ajout des informations de la personne dans le document**/
			Document enregistrement = new Document ();
			enregistrement.append("name", person.getName());
			enregistrement.append("codeName", person.getCodeName());
			enregistrement.append("status",person.getStatus());
			enregistrement.append("imageData",person.getImageData());
			
			/**Pour faire un ajout, il faut que la date de naissance soit valide, que le nom soit unique et que l'ID soit unique aussi**/
			if(isDateValid(person.getDateOfBirth())) {
				
				/*On cherche pour voir si la personne � d�j� �t� enregistr�e*/
				enregistrement.append("dateOfBirth", person.getDateOfBirth());
				BasicDBObject query  = new BasicDBObject();
				query.put("name", new BasicDBObject("$eq",person.getName()));
				
				FindIterable<Document> cursor = collection.find(query);
				Iterator it = cursor.iterator();
				while (it.hasNext()) {
					
					Document result = (Document) it.next();
					Long id_courrant = result.getLong("id");
					person.setId(id_courrant); //Donc si l'enregistrement existe dans Mongo, on set le id de la personne � ajouter
				}

				if (person.getId() != null) { //Si le ID n'est pas null, c'est qu'il a d�j� �t� r�cup�r� avant et que la personne existe d�j� dans les DB Donc on Update
					Document exist = new Document("id", person.getId());
					collection.updateOne( exist, new Document("$set", enregistrement));
				}
				else {
					person.setId(getLatestID());
					enregistrement.append("id", person.getId());
					collection.insertOne(enregistrement);
				}
	
				params.put("p1",person.getName());
				params.put("p2", person.getId());
				
				if (P1 == null) {
					session.run("CREATE (a:person {name: {p1}, id: {p2}})", params);
	                P1 = getPersonNode(person.getName());	
				}
				
				if ((session.run("MATCH (a:person) WHERE a.name = {p1} return a",params)).toString().isEmpty()) {
					Map<String, Object> params2 = new HashMap<String, Object>();
					String nom = P1.get("name").asString();
					params2.put("p1", nom);
					
					StatementResult result = session.run("MATCH (a:person) WHERE a.name = {p1} DETACH a" ,params2);
				}
				
				if (person.getConnexions().size() > 0) {
					for (int i = 0; i < person.getConnexions().size(); i++)
					{
						P2 = getPersonNode(person.getConnexions().get(i));
						if (getRelationship(P1, P2) == null && P2 != null && P1 != P2) {
							 Map<String, Object> params2 = new HashMap<String, Object>();
							 	String n1 = P1.get("name").asString();
							 	String n2 = P2.get("name").asString();
				                params2.put("p1", n1);
				                params2.put("p2", n2);
				                params2.put("p3", "connait" );
				                session.run("MATCH (a:person),(b:person) WHERE a.name = {p1} AND b.name = {p2} CREATE (a)-[r:CONNAIT { desc : {p3} } ]->(b)" , params2);
				                session.run("MATCH (a:person),(b:person) WHERE a.name = {p1} AND b.name = {p2} CREATE (b)-[r:CONNAIT { desc : {p3} } ]->(a)" , params2);       
						}			
					}
				}
				success = true;
			}
		}	
		catch (Exception e) {
			e.printStackTrace();
		}

		
		
		return success;
	}
	
	/**
	 * Recherche d'une personne selon son nom
	 * 
	 * @param name
	 * @return null si aucune personne n'a ce nom, sinon retourne un objet Person
	 */
	private static Person getPerson(String name) {
		Person pers1 = null;
		List<Person> temp = getPeopleList(null,false,(int)getPeopleCount());
		for (int i = 0 ; i < getPeopleCount() ; i++){
			if(temp.get(i).getName().equals(name)) {
				pers1 = temp.get(i);
			}
		}
		return pers1;
		
	}
	
	/**
	 * Recherche du prochain ID a utiliser
	 * 
	 * @return un long dependamment des autres id dans la base de donnees
	 */
	private static long getLatestID() {
		Session s = Neo4jConnection.getConnection();
		String queryNeo = "MATCH (a:person) RETURN a.id ORDER BY a.id DESC";
		long id ;
		if(getPeopleCount() > 0) {
			StatementResult resultNeo = s.run(queryNeo);
			Record recordNeo = resultNeo.next();
			id = recordNeo.get("a.id").asLong() ;
			id += 1 ;
		}
		else 
			id = (long) 1;
		return id;
	}
	
	/**
	 * Acces a un Node dans Neo4j selon le nom
	 * 
	 * @param name
	 * @return l'objet Node correspondant au nom, si c'est impossible retourne un Node null
	 */
	private static Node getPersonNode(String name) {
		Node node = null;
		try {
            Session session= Neo4jConnection.getConnection();
            
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("p1",name);         

            StatementResult result = session.run("MATCH (a:person) WHERE a.name = {p1} RETURN a", params);
            
            while(result.hasNext()) {
                Record record = result.next();
                node = record.get("a").asNode();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
						
		return node;
	}
	
	/**
	 * Acces a une relation entre deux Node dans Neo4j selon leur id
	 * 
	 * @param Node P1, Node P2
	 * @return la relation entre les deux Node, si c'est impossible retourne une relation null
	 */
	public static Relationship getRelationship(Node P1, Node P2) {
        Relationship r = null;
        try {
            Session session= Neo4jConnection.getConnection();
            
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("p1", P1.get("id").asInt());
            
            params.put("p2", P2.get("id").asInt());
            params.put("p3", "connait");
            										

            StatementResult result = session.run("MATCH (a:person)-[r:CONNAIT { desc: {p3} } ]->(b:person) WHERE a.id = {p1} AND b.id = {p2} RETURN r", params);
            
            while(result.hasNext()) {
                Record record = result.next();
                r = record.get("r").asRelationship();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return r;
    }
	
	
	/**
	 * Suppression des donn�es/fiche d'une personne
	 * 
	 * @param person
	 * @return true si succ�s, false sinon
	 */
	public static boolean delete(Person person) {
		boolean success = true;
		try {
			MongoDatabase connection = MongoConnection.getConnection();
			MongoCollection<Document> collection = connection.getCollection("personnes");
			collection.deleteOne(Filters.eq("name", person.getName()));	
			Session session = Neo4jConnection.getConnection();
			Node P = getPersonNode(person.getName());
			Map<String, Object> params = new HashMap<String, Object>();
            params.put("p", P.get("name").asString());
            session.run("MATCH (a:person) WHERE a.name = {p} DETACH DELETE (a) ", params);
			
		}
			catch (Exception e) {
				success = false;
				e.printStackTrace();
			}
	
		return success;
	}
	
	/**
	 * Suppression totale de toutes les donn�es du syst�me!
	 * 
	 * @return true si succ�s, false sinon
	 */
	public static boolean deleteAll() {
		boolean success = true;
		try {
			MongoDatabase connection = MongoConnection.getConnection();
			connection.drop();
			Session session = Neo4jConnection.getConnection();						
			StatementResult result = session.run("MATCH (N) DETACH DELETE N ");
			
		}
			catch (Exception e) {
				e.printStackTrace();
			}
	
		return success;
	}
	
	/**
	 * M�thode qui retourne le ratio de personnes en libert� par rapport au nombre total de fiches.
	 * 
	 * @return ratio entre 0 et 100
	 */
	public static int getFreeRatio() {
		int num = 0;
		long freePeeps = 0;
		long totalPeeps  = getPeopleCount();
		MongoDatabase connection = MongoConnection.getConnection();
		MongoCollection<Document> collection = connection.getCollection("personnes");
		BasicDBObject query  = new BasicDBObject();
		query.put("status", new BasicDBObject("$eq","Libre"));
		FindIterable<Document> cursor = collection.find(query);
		Iterator it = cursor.iterator();
		while (it.hasNext()) {
			Document result = (Document) it.next();
			freePeeps += 1;
		}

		
		num = (int)((freePeeps * 100) / totalPeeps  );

		return num;
	}
	
	/**
	 * Nombre de photos actuellement sauvegard�es dans le syst�me
	 * @return nombre
	 */
	public static long getPhotoCount() {
		MongoDatabase connection = MongoConnection.getConnection();
		MongoCollection<Document> collection = connection.getCollection("personnes");
		long total = collection.count();
		
		return total;
	}
	
	/**
	 * Nombre de fiches pr�sente dans la base de donn�es
	 * @return nombre
	 */
	public static long getPeopleCount() {
		MongoDatabase connection = MongoConnection.getConnection();
		MongoCollection<Document> collection = connection.getCollection("personnes");
		long total = collection.count();
		
		return total;
	}
		
	/**
	 * Permet de savoir la personne la plus jeune du syst�me
	 * 
	 * @return nom de la personne
	 * @throws ParseException 
	 */
	public static String getYoungestPerson()  {
		String oldestDate = "1800-01-01";
		Person min = null ;  
		try {
			Date d0 =  new SimpleDateFormat(DATE_FORMAT).parse(oldestDate);

			
			long Agemin = d0.getTime()/1000 ; 
			List<Person> listPersons = getPeopleList(null, false, (int) getPeopleCount());
			for(int i = 0; i< getPeopleCount(); i ++) {
				Person temp = listPersons.get(i);
				String date = temp.getDateOfBirth();
				
				Date d1;
				d1 = new SimpleDateFormat(DATE_FORMAT).parse(date);
				long seconds = d1.getTime()/1000;			
				if(seconds > Agemin) {
					Agemin = seconds;
					min = listPersons.get(i);
				}
			}	
		} catch (ParseException e1) {
			
			e1.printStackTrace();
		}
		return "-- " + min.getName().toString();
	}
	
	/**
	 * Afin de savoir la prochaine personne � investiguer,
	 * Il faut retourner la personne qui connait, ou est connu, du plus grand nombre de personnes 
	 * disparues ou d�c�d�es (morte). Cette personne doit �videmment avoir le statut "Libre"
	 * 
	 * @return nom de la personne
	 */
	public static String getNextTargetName() {
		List<Person> listePeeps = getPeopleList(null,false,(int)getPeopleCount());
		
		Person target = null;
		int Killcount = 0 ;
		int KillcountTemp = 0 ;
		
		for(int i = 0 ; i < getPeopleCount(); i++) {
			Person persTemp1 = listePeeps.get(i);
			if(persTemp1.getStatus().equals("Libre")) {
				for(int j = 0 ; j < persTemp1.getConnexions().size(); j++)
				{
					List<String> connex = persTemp1.getConnexions();
					if(getPerson(connex.get(j)).getStatus().equals("Disparu") || getPerson(connex.get(j)).getStatus().equals("Mort") ) {
						KillcountTemp ++;
						if(KillcountTemp > Killcount)
							target = persTemp1;
					}
				}
			}
		}
		return (target == null)? ("--"): ("-- " + target.getName());
	}
	
	/**
	 * Permet de retourner, l'�ge moyen des personnes
	 * 
	 * @return par exemple, 20 (si l'�ge moyen est 20 ann�es arrondies)
	 * @throws ParseException 
	 */
	public static int getAverageAge() {
		int resultat = 0;
		long AgeTotal = 0;
		Calendar aujourdhuiCalendar = Calendar.getInstance();
		Calendar DOB = Calendar.getInstance();
		List<Person> listPersons = getPeopleList(null, false, (int) getPeopleCount());
		for(int i = 0; i < getPeopleCount(); i ++) {
			Person temp = listPersons.get(i);
			String date = temp.getDateOfBirth();
			Date d1;
			try {
				d1 = new SimpleDateFormat(DATE_FORMAT).parse(date);
				DOB.setTime(d1);
				
				AgeTotal += (aujourdhuiCalendar.get(Calendar.YEAR ) - DOB.get(Calendar.YEAR));
			} 
			catch (ParseException e) {
				
				e.printStackTrace();
				}
			
			}
		
		resultat = (int)(AgeTotal / getPeopleCount());
		
		return resultat;
	}
	

	/**
	 * Validation des dates
	 * Fonction de validation de la date prise au https://stackoverflow.com/questions/226910/how-to-sanity-check-a-date-in-java
	 * (Tout credit est reserve a l'utilisateur jackal)
	 * 
	 * @param date
	 * @return true si la date est valide, sinon false
	 */
	public static boolean isDateValid(String date) 
	{
	        try {
	            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
	            df.setLenient(false);
	            df.parse(date);
	            return true;
	        } catch (ParseException e) {
	            return false;
	        }
	}
}

