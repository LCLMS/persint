TP Final du cours B55 par Tymothei Lanouette et Clement Salino

Tout d'abord, nous avons choisi d'utiliser Neo4j pour enregistrer les connexions entre
les personnes car c'est un outil qui est particulierment adapte a ce genre de situation
et qui fourni une interface intuitives pour visualiser ces relations.

Ensuite, nous avons choisi d'utiliser MongoDB pour enregistrer toutes les informations
des restantes a propos des personnes.

Relation Neo4j :
 personne1 <-connait-> personne2

MongoDB collection personnes:
 name
 codeName
 status
 imageData
 dateOfBirth
 id